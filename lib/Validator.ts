import { Redeem, RedeemRequest, UserTransaction } from "xand-api-ts-client/dist/xand-api_pb";
import { XngValidatorMetadata } from "./config/xng/XngValidatorMetadata";
import { EnhancedXandApi } from "./EnhancedXandApi";

const MCB_SHORTNAME = "mcb";

export default class Validator {
  public static fromMetadata(metadata: XngValidatorMetadata): Validator {
    const apiDetails = metadata.xandApiDetails;

    const result = new Validator();
    result.xandApi = new EnhancedXandApi(apiDetails.xandApiUrl, apiDetails.auth.token);
    result.address = metadata.address;

    const mcbAccount = metadata.banks.find((bank) => bank.bank === MCB_SHORTNAME).account;
    result.mcbBankAccount = mcbAccount.accountNumber.toString();
    result.mcbBankRouting = mcbAccount.routingNumber.toString();

    return result;
  }

  public xandApi: EnhancedXandApi;
  public address: string;
  public mcbBankAccount: string;
  public mcbBankRouting: string;

  public async getBalance(): Promise<number> {
    return this.xandApi.getBalance(this.address);
  }

  public async redeem(amount: number, customBankAccount?: string):
      Promise<{ correlationId: string, redeemRequest: string }> {
    return this.xandApi.redeemRequestClaim(
        this.address,
        customBankAccount || this.mcbBankAccount,
        this.mcbBankRouting,
        amount,
    );
  }

  public async awaitRedeemCompletion(redeemRequestTransactionId: string) {
    await this.xandApi.awaitRedeemCompletion(redeemRequestTransactionId);
  }

  public async getRedeemRequest(redeemRequestTransactionId: string): Promise<Redeem> {
    const transaction = await this.xandApi.getAsyncTransactionById(redeemRequestTransactionId);
    return transaction.getTransaction().getRedeemRequest();
  }
}
