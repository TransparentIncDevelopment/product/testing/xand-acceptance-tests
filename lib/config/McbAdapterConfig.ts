export class McbAdapterConfig {
    public url: string;
    public secretUserName: string;
    public secretPassword: string;
    public secretClientAppIdent: string;
    public secretOrganizationId: string;
}
