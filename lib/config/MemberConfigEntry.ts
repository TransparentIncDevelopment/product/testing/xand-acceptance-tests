import { AccountEntry } from "./AccountEntry";
import { BankConfigEntry } from "./BankConfigEntry";
import { XngMemberMetadata } from "./xng/XngMemberMetadata";

export class MemberConfigEntry {
    public id: string;
    public xngIndex: number;
    public banks: BankConfigEntry[];
    public xngMemberMetadata: XngMemberMetadata;
}

/// Filters metadata to return accounts at this bank
export function getAccountsForBank(
    memberConfigEntry: MemberConfigEntry,
    bankName: string): AccountEntry[] {
    const lowerCaseBankName = bankName.toLowerCase();

    // Note: The key `xngMemberMetadata.banks` is a list of bank accounts
    const xngAccounts = memberConfigEntry.xngMemberMetadata.banks.filter(
        (bankAccount) =>
            bankAccount.bank.toLowerCase() === lowerCaseBankName,
    );

    return xngAccounts.map((xngAccount, index) => {
        const fullAccountInfo: AccountEntry = {
            shortName: `${memberConfigEntry.id}_${lowerCaseBankName}_${index}`,
            xngAccountMetadata: xngAccount.account,
        };
        return fullAccountInfo;
    });
}
