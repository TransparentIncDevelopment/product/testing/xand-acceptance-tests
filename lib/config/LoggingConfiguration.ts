import { ErrorLoggingConfiguration } from "./ErrorLoggingConfiguration";

export default class LoggingConfiguration {
  public url: boolean;
  public request: boolean;
  public response: boolean;
  public error: ErrorLoggingConfiguration;
}
