import { XngMemberApiDetails } from "./XngMemberApiDetails";
import { XngMemberBanksMetadata } from "./XngMemberBanksMetadata";
import { XngXandApiDetails } from "./XngXandApiDetails";

export class XngMemberMetadata {
    public xandApiDetails: XngXandApiDetails;
    public address: string;
    public memberApiDetails: XngMemberApiDetails;
    public banks: XngMemberBanksMetadata[];
}
