import { XngMemberAccountMetadata } from "./XngMemberAccountMetadata";

export class XngMemberBanksMetadata {
    public bank: string;
    public account: XngMemberAccountMetadata;
}
