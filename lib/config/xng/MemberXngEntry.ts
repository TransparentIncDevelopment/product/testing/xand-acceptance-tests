import { BankConfigEntry } from "../BankConfigEntry";

export default class MemberXngEntry {
    public address: string;
    public url: string;
    public banks: BankConfigEntry[];
}
