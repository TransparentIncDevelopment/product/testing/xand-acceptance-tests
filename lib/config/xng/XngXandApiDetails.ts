import { XngJwtInfo } from "./XngJwtInfo";

export class XngXandApiDetails {
    public xandApiUrl: string;
    public auth: XngJwtInfo | null;
}
