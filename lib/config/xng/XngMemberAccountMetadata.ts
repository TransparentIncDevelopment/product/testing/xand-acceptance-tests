export class XngMemberAccountMetadata {
    public routingNumber: string;
    public accountNumber: string;
}
