import camelcaseKeys from "camelcase-keys";
import fs from "fs";
import YAML from "yaml";
import { XngBanksMetadata } from "./XngBanksMetadata";
import { XngMemberMetadata } from "./XngMemberMetadata";
import { XngTrustMetadata } from "./XngTrustMetadata";
import { XngValidatorMetadata } from "./XngValidatorMetadata";

export default class XngMetadata {

    public static load_default(): XngMetadata {
        const xngMetadataFile = "generated/entities-metadata.yaml";
        const xngMetadataContents = fs.readFileSync(xngMetadataFile, "utf8");
        const metadataParsed = YAML.parse(xngMetadataContents);
        const metadataCamelCase = camelcaseKeys(metadataParsed, { deep: true });
        return XngMetadata.fromMap(metadataCamelCase);
    }

    public static fromMap(input: any): XngMetadata {
        const xngMetaData = Object.assign(new XngMetadata(), input);
        xngMetaData.validator = input.validators[0];
        return xngMetaData;
    }

    public banks: XngBanksMetadata;
    public trust: XngTrustMetadata;
    public members: XngMemberMetadata[];
    public validator: XngValidatorMetadata;

}
