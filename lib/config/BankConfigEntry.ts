import BankAdapterConfig from "./BankAdapterConfig";

export class BankConfigEntry {
  public name: string;
  public routingNumber: string;
  public claimsAccount: string;
  public adapter: BankAdapterConfig;
}
