export class RouteMatcher {
  public url: string;
  public verb: "GET" | "PUT" | "POST" | "DELETE" | "PATCH";
  public status: number[];
}
