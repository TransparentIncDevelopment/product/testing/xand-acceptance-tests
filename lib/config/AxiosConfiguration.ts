import Axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from "axios";
import { addLogger } from "axios-debug-log";
import colors from "colors";
import debug from "debug";
import * as prettyjson from "prettyjson";
import Configuration from "./Configuration";
import { ErrorLoggingConfiguration } from "./ErrorLoggingConfiguration";
import LoggingConfiguration from "./LoggingConfiguration";

// tslint:disable: no-console

const axiosDebug = debug("axios");
const config = Configuration.loadTestEnvironment();
const loggingConfiguration: LoggingConfiguration = config.logging;
const errorLogging: ErrorLoggingConfiguration = Object.assign(
  new ErrorLoggingConfiguration(),
  config.logging.error,
);

export function createAxiosInstance(): AxiosInstance {
  const axiosInstance = Axios.create();
  modifyErrorHandling(axiosInstance);
  addLogger(axiosInstance, axiosDebug);
  return axiosInstance;
}

function modifyErrorHandling(axiosInstance: AxiosInstance) {
  axiosInstance.interceptors.request.use(
    (request: AxiosRequestConfig) => {
      if (config.logging.url) {
        console.info(
          colors.yellow(`${request.method.toUpperCase()} ${request.url}`),
        );
      }

      if (config.logging.request) {
        if (request.data) {
          console.log(
            colors.magenta(
              "REQUEST BODY:\n----------------------------------------",
            ),
          );
          console.log(prettyjson.render(JSON.parse(request.data)));

          if (!config.logging.response) {
            console.log("\n");
          }
        }
      }

      return request;
    },
    (error: AxiosError) => {
      throw error;
    },
  );

  axiosInstance.interceptors.response.use(
    (response: AxiosResponse) => {
      if (loggingConfiguration.response) {
        console.log(
          colors.cyan(
            `${response.request.method} ${response.config.url} -> ${response.status} - ${response.statusText}`,
          ),
        );
        console.log(
          colors.magenta(
            "RESPONSE BODY:\n----------------------------------------",
          ),
        );
        console.log(prettyjson.render(response.data));
        console.log("\n");
      }

      return response;
    },
    (error: AxiosError) => {
      if (error) {
        if (
          errorLogging.shouldLog(
            error.request.method,
            error.config.url,
            error.response.status,
          )
        ) {
          if (error.response) {
            console.log(
              colors.red(
                `${error.request.method} ${error.config.url} -> ${error.response.status} - ${error.response.statusText}`,
              ),
            );

            console.log(colors.red(error.message));
            console.log(prettyjson.render(error.response.data));
            console.log("\n");
          } else {
            console.log(colors.red(error.message));
          }
        }

        throw error;
      }
    },
  );
}
