import camelcaseKeys from "camelcase-keys";
import { expect } from "chai";
import fs from "fs";
import YAML from "yaml";
import mergeDeep from "./extend";
import LoggingConfiguration from "./LoggingConfiguration";
import { MemberConfigEntry } from "./MemberConfigEntry";
import Polling from "./Polling";
import XngMetadata from "./xng/XngMetadata";
import { XngTrustMetadata } from "./xng/XngTrustMetadata";
import { XngValidatorMetadata } from "./xng/XngValidatorMetadata";

export default class Configuration {
  public static environment(): string {
    let result = "local";

    process.argv.forEach((element, index) => {
      const str = process.argv[index];
      if (str === "--test-env") {
        result = process.argv[index + 1];
      }
    });

    return result;
  }

  public static loadTestEnvironment(): Configuration {
    const environment = Configuration.environment();
    const result = Configuration.default();
    if (environment !== "local") {
      const overrideFileName = `config/config.${environment}.yaml`;
      const override = Configuration.configFromFile(overrideFileName);
      mergeDeep<Configuration>(result, override);
    }
    return result;
  }

  public static override(overrideFile: string): Configuration {
    const configuration = Configuration.default();
    const overrideFileName = `config/config.${overrideFile}.yaml`;
    const config = fs.readFileSync(overrideFileName, "utf8");
    const parsed = YAML.parse(config);
    const camelCaseOverrides = camelcaseKeys(parsed, { deep: true });

    const result = mergeDeep<Configuration>(configuration, camelCaseOverrides);

    return result;
  }

  public static configFromFile(filename: string): Configuration {
    const config = fs.readFileSync(filename, "utf8");
    const parsed = YAML.parse(config);
    const camelCaseOverrides = camelcaseKeys(parsed, { deep: true });
    return camelCaseOverrides;
  }

  public static fromMap(input: any): Configuration {
    return Object.assign(new Configuration(), input);
  }

  public static default(): Configuration {
    const configFile = "config/config.yaml";
    const config = fs.readFileSync(configFile, "utf8");
    const parsed = YAML.parse(config);
    const camelCase = camelcaseKeys(parsed, { deep: true });
    const result = Configuration.fromMap(camelCase);

    const xngMetadata = XngMetadata.load_default();
    result.xngMetadata = xngMetadata;

    return result;
  }

  public members: MemberConfigEntry[];
  public validator: XngValidatorMetadata;
  public logging: LoggingConfiguration;
  public polling: Polling;
  public xngMetadata: XngMetadata;

  public getAwesomeCo(): MemberConfigEntry {
    return this.getMemberById("awesomeco");
  }

  public getCoolInc(): MemberConfigEntry {
    return this.getMemberById("coolinc");
  }

  public getMemberById(id: string): MemberConfigEntry {
    const memberConfigEntry: MemberConfigEntry = this.members.find((member) => member.id === id);
    expect(memberConfigEntry).to.not.be.undefined;
    const xngMemberMetadata = this.xngMetadata.members[memberConfigEntry.xngIndex];
    expect(xngMemberMetadata).to.not.be.undefined;
    memberConfigEntry.xngMemberMetadata = xngMemberMetadata;

    return memberConfigEntry;
  }

  public getTrust(): XngTrustMetadata {
    const trustMetadata: XngTrustMetadata = this.xngMetadata.trust;
    expect(trustMetadata).to.not.be.undefined;
    return trustMetadata;
  }

  public getValidator(): XngValidatorMetadata {
    const validatorMetadata: XngValidatorMetadata = this.xngMetadata.validator;
    expect(validatorMetadata).to.not.be.undefined;
    return validatorMetadata;
  }
}
