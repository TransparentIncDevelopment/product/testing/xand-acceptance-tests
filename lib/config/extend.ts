import * as lodash from "lodash";

export function extend<T>(a: any, b: any): T {
  for (const key in b) {
    if (b.hasOwnProperty(key)) {
      a[key] = b[key];
    }
  }
  return a;
}

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export function isObject(item) {
  return item && typeof item === "object" && !Array.isArray(item);
}

export function isArray(item) {
  return Array.isArray(item);
}

/**
 * Deep merge two objects.
 * @param target
 * @param ...sources
 */
export default function mergeDeep<T>(target, ...sources): T {
  return lodash.merge(target, ...sources);
}
