import { RouteMatcher } from "./RouteMatcher";

export class ErrorLoggingConfiguration {

  public enabled: boolean;
  public exclude: RouteMatcher[];
  public shouldLog(
    method: "GET" | "PUT" | "POST" | "DELETE",
    url: string,
    status: number,
  ): boolean {
    if (!this.enabled) { return false; }

    const exclusion = this.exclude?.find(
      (item) =>
        url.match(item.url) &&
        item.verb === method &&
        item.status?.includes(status),
    );
    return exclusion ? false : true;
  }
}
