import { AxiosError, AxiosPromise } from "axios";
import urlJoin from "url-join";
import {
  AccountsApi,
  Bank,
  BankAccount,
  BankAccountBalance,
  BanksApi,
  Configuration,
  FundClaimsReceipt,
  FundClaimsRequest,
  MemberApi,
  NewAccount,
  Receipt,
  Retrier, ServiceApi,
  ServiceHealthResponse,
  Transaction,
  TransactionHistory,
  TransactionsApi,
  TransactionsPollerApi,
  UpdateBank,
  WorkflowApi, XandBalance,
} from "xand-member-api-client";
import { AccountEntry } from "./config/AccountEntry";
import { createAxiosInstance } from "./config/AxiosConfiguration";
import { getAccountsForBank, MemberConfigEntry } from "./config/MemberConfigEntry";
import Polling from "./config/Polling";

export default class Member {

  public static fromConfig(
    config: MemberConfigEntry,
    polling: Polling,
  ): Member {
    if (!polling) {
      throw new Error("Polling settings are required.");
    }

    const apiConfiguration = new Configuration();

    const auth = config.xngMemberMetadata.memberApiDetails.auth;
    if (auth) {
      apiConfiguration.baseOptions = { headers: { Authorization: `Bearer ${auth.token}` } };
    }

    const basePath = urlJoin(config.xngMemberMetadata.memberApiDetails.memberApiUrl, "api/v1");
    apiConfiguration.basePath = basePath;

    const axios = createAxiosInstance();

    const result = new Member();
    result.config = config;
    result.accountsApi = new AccountsApi(apiConfiguration, undefined, axios);
    result.banksApi = new BanksApi(apiConfiguration, undefined, axios);
    result.memberApi = new MemberApi(apiConfiguration, undefined, axios);
    result.transactionsApi = new TransactionsApi(apiConfiguration, undefined, axios);
    result.transactionsApi = new TransactionsApi(
      apiConfiguration,
      undefined,
      axios,
    );
    result.retrier = new Retrier(polling.retries, polling.delay);
    result.transactionsPollerApi = new TransactionsPollerApi(result.transactionsApi);

    result.workflowApi = new WorkflowApi(result.transactionsApi, result.accountsApi);
    result.address = config.xngMemberMetadata.address;
    result.serviceApi = new ServiceApi(apiConfiguration, undefined, axios);
    return result;
  }

  public config: MemberConfigEntry;

  public accountsApi: AccountsApi;
  public banksApi: BanksApi;
  public memberApi: MemberApi;
  public transactionsApi: TransactionsApi;
  public retrier: Retrier;
  public transactionsPollerApi: TransactionsPollerApi;
  public name: string;
  public address: string;
  public workflowApi: WorkflowApi;
  public selectedAccount: BankAccount;
  public serviceApi: ServiceApi;

  public banks: Bank[];
  public accounts: BankAccount[];

  public async DeleteAllBanks(): Promise<Member> {
    const allAccounts = (await this.accountsApi.getAccounts()).data;
    const allBanks = (await this.banksApi.getBanks()).data;

    const removeAccounts = allAccounts.map((account) =>
      this.accountsApi.deleteAccount(account.id),
    );
    await Promise.all(removeAccounts);

    const removeBanks = allBanks.map((bank) => this.banksApi.deleteBank(bank.id));
    await Promise.all(removeBanks);

    return this;
  }

  public async EnsureBanksAreConfigured(): Promise<Member> {
    let apiBanks = (await this.banksApi.getBanks()).data;
    let apiAccounts = (await this.accountsApi.getAccounts()).data;

    let refreshBanks: boolean = false;
    let refreshAccounts: boolean = false;

    // Ensure Banks are configured
    const banks = this.config.banks.map((bankConfigEntry) => {
      const found = apiBanks.find(
        (bank) => bank.name === bankConfigEntry.name,
      );

      if (!found) {
        refreshBanks = true;
        const body: UpdateBank = {
          name: bankConfigEntry.name,
          routingNumber: bankConfigEntry.routingNumber,
          claimsAccount: bankConfigEntry.claimsAccount,
          adapter: bankConfigEntry.adapter,
        };
        return this.banksApi.postBanks(body);
      }
    });
    await Promise.all(banks);

    if (refreshBanks) { apiBanks = (await this.banksApi.getBanks()).data; }
    // Ensure Accounts per Bank are configured
    const accountPromises = [];

    this.config.banks.forEach((bankConfigEntry) => {
      // Get first bank with matching name for this account
      const bank = apiBanks.filter(
        (e) => e.name === bankConfigEntry.name,
      )[0];

      // Get accounts for this bank
      const accounts: AccountEntry[] = getAccountsForBank(this.config, bankConfigEntry.name);

      accounts.map((accountConfigEntry) => {
        // Compare shortName of Account to check that its configured.
        const matching = apiAccounts.find(
          (account) => account.shortName === accountConfigEntry.shortName,
        );

        if (!matching) {
          refreshAccounts = true;
          const insert: NewAccount = {
            bankId: bank.id,
            shortName: accountConfigEntry.shortName,
            accountNumber: accountConfigEntry.xngAccountMetadata.accountNumber.toString(),
          };
          accountPromises.push(this.accountsApi.postAccounts(insert));
        }
      });
    });
    await Promise.all(accountPromises);

    if (refreshAccounts) {
      apiAccounts = (await this.accountsApi.getAccounts()).data;
    }

    this.banks = apiBanks;
    this.accounts = apiAccounts;

    this.accounts = (await this.accountsApi.getAccounts()).data
      .sort((a, b) => a.shortName.localeCompare(b.shortName))
    ;

    this.selectAccountByName(this.accounts[0].shortName);

    return this;
  }

  public selectAccountByName(name: string): BankAccount {
    if (this.accounts == null) {
      throw new Error("Did you call EnsureBanksAreConfigured()?");
    }

    const found = this.accounts.find((account) => account.shortName === name);
    if (!found) {
      throw new Error(`Could not find account with name="${name}" in the list :${JSON.stringify(this.accounts)}`);
    }
    this.selectedAccount = found;
    return this.selectedAccount;
  }

  public async HasABalanceOfAtLeast(amount: number): Promise<boolean> {
    const response = (await this.memberApi.getMemberBalance()).data;
    return response.balanceInMinorUnit > amount;
  }

  public async SubmitCreateRequest(amount: number): Promise<Receipt> {
    const account = this.getSelectedAccount();
    if (!account) {
      throw new Error(`No account selected for Member ${this.name}`);
    }

    return await this.workflowApi.createAndWait({
      accountId: account.id,
      amount,
    });
  }

  public getSelectedAccount(): BankAccount {
    return this.selectedAccount;
  }

  public async SubmitRedeemRequest(
    amount: number,
    accountId: number = this.getSelectedAccount().id,
  ): Promise<Receipt> {
    return await this.workflowApi.redeemAndWait({
      address: this.address,
      accountId,
      amount,
    });
  }

  public async SubmitSend(to: string, amount: number): Promise<Receipt> {
    return await this.workflowApi.payAndWait({
      toAddress: to,
      fromAddress: this.address,
      amount,
    });
  }

  public async SubmitTransferToReserve(
    amount: number,
    receipt: Receipt,
  ): Promise<FundClaimsReceipt> {

    const account = this.getSelectedAccount();
    if (!account) {
      throw new Error(`No account selected for Member ${this.name}`);
    }

    const transferRequest: FundClaimsRequest = {
      amountInMinorUnit: amount,
      correlationId: receipt.correlationId,
      bankAccountId: account.id,
    };

    return (
      await this.transactionsApi.fundClaimsTransaction(
        receipt.transactionId,
        transferRequest,
      )
    ).data;
  }

  public async SetWalletBalanceToAtLeast(amount: number) {
    if (await this.HasABalanceOfAtLeast(amount)) {
      return;
    }

    const receipt = await this.SubmitCreateRequest(amount);
    await this.SubmitTransferToReserve(amount, receipt);
    await this.transactionsPollerApi.awaitTransactionResolution({
      transactionId: receipt.transactionId,
      retrier: this.retrier,
    });
  }

  public async GetHistory(
    pageNumber?: number,
    pageSize?: number,
  ): Promise<TransactionHistory> {
    return (await this.transactionsApi.getTransactions(pageSize, pageNumber))
      .data;
  }

  public async GetTransactionById(id: string): Promise<Transaction> {
    return (await this.transactionsApi.getTransactionById(id)).data;
  }

  public async pollForTransactionById(id: string): Promise<Transaction> {
    return await this.transactionsPollerApi.awaitTransaction({
      transactionId: id,
      retrier: this.retrier,
    });
  }

  public async awaitConfirmation(id: string): Promise<Transaction> {
    const confirmationTransaction = await this.transactionsPollerApi.awaitTransactionConfirmation(
      { transactionId: id, retrier: this.retrier },
    );
    return await this.transactionsPollerApi.awaitTransactionResolution({
      transactionId: confirmationTransaction.confirmationId,
      retrier: this.retrier,
    });
  }

  public async awaitResolution(id: string): Promise<Transaction> {
    return await this.transactionsPollerApi.awaitTransactionResolution({
      transactionId: id,
      retrier: this.retrier,
    });
  }

  public async GetXandBalance(): Promise<XandBalance> {
    try {
      const balanceRetrieval = await this.memberApi.getMemberBalance();
      return balanceRetrieval.data;
    } catch (err) {
      const error: AxiosError = err as AxiosError;
      if (error) {
        const body =
          error.response?.data?.XandClientError?.source?.GrpcError?.source;
        if (body && body.includes("account likely has not been initialized")) {
          return { balanceInMinorUnit: 0 };
        }
      }

      throw error;
    }
  }

  public async GetBankAccountBalance(
    accountId: number = this.getSelectedAccount().id,
  ): Promise<BankAccountBalance> {
    return (await this.accountsApi.getBankBalance(accountId)).data;
  }

  public async GetHealth(): Promise<ServiceHealthResponse> {
    return (await this.serviceApi.getServiceHealth()).data;
  }
}
