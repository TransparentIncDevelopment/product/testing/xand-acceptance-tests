import Configuration from "./config/Configuration";
import { EnhancedXandApi } from "./EnhancedXandApi";
import Member from "./Member";
import { Trust } from "./Trust";
import Validator from "./Validator";

export default class TestContext {
  public awesomeCo: Member;
  public coolInc: Member;
  public trust: Trust;
  public validator: Validator;
  public configuration: Configuration;

  constructor() {
    this.configuration = Configuration.loadTestEnvironment();
    const awesomeCo = this.configuration.getAwesomeCo();
    const coolInc = this.configuration.getCoolInc();
    const trust = this.configuration.getTrust();
    const validator = this.configuration.getValidator();

    this.awesomeCo = Member.fromConfig(awesomeCo, this.configuration.polling);
    this.coolInc = Member.fromConfig(coolInc, this.configuration.polling);
    this.trust = Trust.fromMetadata(trust);
    this.validator = Validator.fromMetadata(validator);
  }

  public GetTrusteeAddress(): string {
    return this.configuration.xngMetadata.trust.address;
  }

  public getAwesomeCoXandApi(): EnhancedXandApi {
    const awesomeCoConfig = this.configuration.getAwesomeCo();
    const details = awesomeCoConfig.xngMemberMetadata.xandApiDetails;
    return new EnhancedXandApi(details.xandApiUrl, details.auth?.token);
  }

  public getCoolIncXandApi(): EnhancedXandApi {
    const coolIncConfig = this.configuration.getCoolInc();
    const details = coolIncConfig.xngMemberMetadata.xandApiDetails;
    return new EnhancedXandApi(details.xandApiUrl, details.auth?.token);
  }

  public getTrustXandApi(): EnhancedXandApi {
    const details = this.configuration.xngMetadata.trust.xandApiDetails;
    return new EnhancedXandApi(details.xandApiUrl, details.auth?.token);
  }

  public getValidator(): Validator {
    return this.validator;
  }
}
