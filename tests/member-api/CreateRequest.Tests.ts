import { expect } from "chai";
import {
    BankAccountBalance,
    FundClaimsReceipt,
    OperationType,
    Receipt,
    Transaction,
    TransactionState,
    XandBalance,
} from "xand-member-api-client";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

describe("Given a Member named Awesomeco who wants to Create XAND", () => {
    const MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED = 120000;
    const TRANSACTION_ID_FORMAT = "^0x([A-Fa-f]|\\d){64}$";

    let context: TestContext;
    let awesomeCo: Member;

    before(async () => {
        context = new TestContext();
        awesomeCo = await context.awesomeCo.EnsureBanksAreConfigured();
        awesomeCo.selectAccountByName("awesomeco_mcb_0");
    });

    describe("When I submit a create request", () => {
        let receipt: Receipt;
        let startingBalance: BankAccountBalance;
        let createRequest: Transaction;
        const amount = 100;

        // Passing arrow functions (“lambdas”) to Mocha is discouraged.
        // Due to the lexical binding of this, such functions are unable
        // to access the Mocha context.
        //
        // Translation: In order to call `this.timeout()` we need to use the
        // `async function` syntax instead of `async () =>` syntax.
        //
        // tslint:disable-next-line: only-arrow-functions
        before(async function() {
            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

            receipt = await awesomeCo.SubmitCreateRequest(amount);
            expect(receipt).to.exist;

            createRequest = await awesomeCo.GetTransactionById(
                receipt.transactionId,
            );
            expect(createRequest).to.exist;

            startingBalance = await awesomeCo.GetBankAccountBalance();
            expect(startingBalance).to.exist;
        });

        it("Then the system gives me a transaction id", () => {
            expect(receipt.transactionId).to.exist;
        });

        it("Then the transaction id is in the correct format", () => {
            const regex = new RegExp(TRANSACTION_ID_FORMAT);
            expect(receipt.transactionId).matches(regex);
        });

        it("Then the system gives me the correlation id", () => {
            expect(receipt.correlationId).to.exist;
        });

        // Passing arrow functions (“lambdas”) to Mocha is discouraged.
        // Due to the lexical binding of this, such functions are unable
        // to access the Mocha context.
        //
        // Translation: In order to call `this.timeout()` we need to use the
        // `async function` syntax instead of `async () =>` syntax.
        //
        // tslint:disable-next-line: only-arrow-functions
        it("Then the transaction history shows the transaction details", async function() {
            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

            const firstTransaction = await awesomeCo.pollForTransactionById(
                receipt.transactionId,
            );

            expect(firstTransaction.operation).to.equal(
                OperationType.CreateRequest,
            );
            expect(firstTransaction.signerAddress).to.equal(awesomeCo.address);
            expect(firstTransaction.amountInMinorUnit).to.equal(amount);
            expect(firstTransaction.destinationAddress).to.equal(awesomeCo.address);
            expect(firstTransaction.correlationId).to.equal(receipt.correlationId);

            expect(firstTransaction.bankAccount).to.exist;
            expect(firstTransaction.bankAccount.id).to.equal(
                awesomeCo.getSelectedAccount().id,
            );
            expect(firstTransaction.bankAccount.bankName).to.equal("MCB");
            expect(firstTransaction.bankAccount.routingNumber).to.equal("121141343");
            expect(firstTransaction.confirmationId).to.be.undefined;
            expect(firstTransaction.cancellationId).to.be.undefined;
        });

        describe("And I get the transaction by id", () => {
            before(async function() {
                this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                createRequest = await awesomeCo.GetTransactionById(
                    receipt.transactionId,
                );
                expect(createRequest).to.exist;
            });

            it("Then the transaction id is in the correct format", () => {
                const regex = new RegExp(TRANSACTION_ID_FORMAT);
                expect(createRequest.transactionId).matches(regex);
            });

            it("Then I can review the transaction status", () => {
                expect(createRequest.transactionId).to.equal(receipt.transactionId);
                expect(createRequest.status.state).to.equal(TransactionState.Pending);
            });

            it("Then the confirmation id is empty", () => {
                expect(createRequest.confirmationId).to.be.undefined;
            });

            it("Then the cancellation id is empty.", () => {
                expect(createRequest.cancellationId).to.be.undefined;
            });

            it("Then I can get the full transaction details", () => {
                expect(createRequest.transactionId).to.equal(receipt.transactionId);
                expect(createRequest.destinationAddress).to.equal(
                    createRequest.destinationAddress,
                );
                expect(createRequest.correlationId).to.equal(createRequest.correlationId);
                expect(createRequest.operation).to.equal(
                    OperationType.CreateRequest,
                );
                expect(createRequest.signerAddress).to.equal(
                    createRequest.signerAddress,
                );
                expect(createRequest.status.state).to.equal(TransactionState.Pending);
                expect(createRequest.status.details).to.not.exist;
                expect(createRequest.datetime).to.exist;
            });

            it("Then I can see the transaction in the history", async () => {
                const match = await awesomeCo.pollForTransactionById(
                    receipt.transactionId,
                );
                expect(match.amountInMinorUnit).to.equal(amount);
                expect(match.destinationAddress).to.equal(awesomeCo.address);
                expect(match.correlationId).to.equal(receipt.correlationId);
                expect(match.signerAddress).to.equal(awesomeCo.address);
                expect(match.operation).to.equal(OperationType.CreateRequest);
            });
        });

        describe("When I transfer funds to the reserve", () => {
            let xandBalance: XandBalance;
            let transfer: FundClaimsReceipt;

            before(async () => {
                xandBalance = await awesomeCo.GetXandBalance();
                transfer = await awesomeCo.SubmitTransferToReserve(
                    createRequest.amountInMinorUnit,
                    receipt,
                );
                expect(transfer).to.exist;
            });

            it("Then the transfer receipt is populated", () => {
                expect(transfer.amountInMinorUnit).to.equal(
                    createRequest.amountInMinorUnit,
                );
            });

            it("Then the bank balance is reduced by the amount of the transfer.", async () => {
                const newBalance = await awesomeCo.GetBankAccountBalance();
                const expectedBalance =
                    startingBalance.currentBalanceInMinorUnit -
                    createRequest.amountInMinorUnit;

                expect(newBalance.currentBalanceInMinorUnit).to.equal(expectedBalance);
                expect(newBalance.availableBalanceInMinorUnit).to.equal(
                    expectedBalance,
                );
            });
        });
    });
});
