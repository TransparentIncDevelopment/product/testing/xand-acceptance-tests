import { expect } from "chai";

import {
    AccountsApi,
    BankAccount,
} from "xand-member-api-client";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

describe("Given a provident bank account", () => {

    let context: TestContext;
    let providentAccount: BankAccount;
    const providentBankAccountName = "awesomeco_provident_0";

    before(async () => {
        context = new TestContext();
        await context.awesomeCo.EnsureBanksAreConfigured();
        providentAccount = context.awesomeCo.selectAccountByName(providentBankAccountName);
    });

    describe("Given a banks api", () => {

        let accountsApi: AccountsApi;

        before(() => {
            accountsApi = context.awesomeCo.accountsApi;
        });

        // tslint:disable-next-line:only-arrow-functions
        describe("When I get balances", function() {
            it("Then current and available balances are greater than zero", async () => {
                const data = (await accountsApi.getBankBalance(providentAccount.id)).data;
                expect(data.currentBalanceInMinorUnit).to.be.greaterThan(0);
                expect(data.availableBalanceInMinorUnit).to.be.greaterThan(0);
            });
        });
    });
});
