import { expect } from "chai";
import { ServiceHealthStatus } from "xand-member-api-client";
import TestContext from "../../lib/TestContext";

describe("Given a Member API in a running XAND network", () => {
    let context: TestContext;

    before(() => {
        context = new TestContext();
    });

    describe("When I probe the health endpoint", () => {
        // Passing arrow functions (“lambdas”) to Mocha is discouraged.
        // Due to the lexical binding of this, such functions are unable
        // to access the Mocha context.
        //
        // Translation: In order to call `this.timeout()` we need to use the
        // `async function` syntax instead of `async () =>` syntax.
        //
        // tslint:disable-next-line: only-arrow-functions
        let healthResponse;
        before(async () => {
            healthResponse = await context.awesomeCo.GetHealth();
        });

        it("Then the health endpoint reports healthy", () => {
            expect(healthResponse.down).to.be.empty;
        });
        it("Then the health endpoint reports all the services are up", () => {
            const isUp = (serviceHealth) => serviceHealth.status === ServiceHealthStatus.Up;
            expect(healthResponse.up.every(isUp)).to.be.true;
        });

    });
});
