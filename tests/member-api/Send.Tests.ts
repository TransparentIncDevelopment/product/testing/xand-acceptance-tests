import { AxiosError } from "axios";
import { expect } from "chai";
import {
    Receipt,
    Transaction,
    TransactionState,
} from "xand-member-api-client";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

describe("Given a wallet with $1", () => {
    const MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED = 120000;
    const MAX_TIME_TO_WAIT_ON_FULFILLMENT = 120000;
    const TRANSACTION_ID_FORMAT = "^0x([A-Fa-f]|\\d){64}$";

    let context: TestContext;
    let sender: Member;
    let receiver: Member;

    before(async function() {
        this.timeout(MAX_TIME_TO_WAIT_ON_FULFILLMENT);

        context = new TestContext();
        sender = await context.awesomeCo.EnsureBanksAreConfigured();
        receiver = context.coolInc;

        await sender.SetWalletBalanceToAtLeast(100);
    });

    describe("When I send claims", () => {

        let send: Transaction;
        let to: string;

        let senderBalance: number;
        let receiverBalance: number;
        let sendAmount: number;
        let receipt: Receipt;

        before(async function() {
            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
            to = receiver.address;
            senderBalance = (await sender.GetXandBalance()).balanceInMinorUnit;
            receiverBalance = (await receiver.GetXandBalance()).balanceInMinorUnit;
            sendAmount = Math.floor(senderBalance / 2);

            receipt = await sender.SubmitSend(to, sendAmount);

            send = await sender.GetTransactionById(receipt.transactionId);
        });

        it("Then the send was successful", () => {
            expect(send).to.exist;
            expect(send.status.state).to.equal(TransactionState.Confirmed);
        });

        it("Then the transaction id is expressed in correct format", () => {
            const regex = new RegExp(TRANSACTION_ID_FORMAT);
            expect(send.transactionId).to.match(regex);
        });

        it("Then the confirmation id is empty", () => {
            expect(send.confirmationId).to.be.undefined;
        });

        it("Then the cancellation id is empty.", () => {
            expect(send.cancellationId).to.be.undefined;
        });

        it(
            "Then the receiver's XAND balance was increased by the amount of the send",
            async function() {
                this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                await receiver.awaitResolution(receipt.transactionId);
                const startingBalance = receiverBalance;
                const endingBalance = (await receiver.GetXandBalance()).balanceInMinorUnit;
                expect(endingBalance - startingBalance).to.equal(sendAmount);
            },
        );

        it(
            "Then the sender's XAND balance was reduced by the amount of the send",
            async function() {
                this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                const startingBalance = senderBalance;
                const endingBalance = (await sender.GetXandBalance()).balanceInMinorUnit;
                expect(endingBalance - startingBalance).to.equal(-sendAmount);
            },
        );
    });

    describe("When I send claims with more Xand than I have", () => {

        let send: Transaction;
        let to: string;
        let balance: number;
        let error: AxiosError;

        before(async function() {
            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

            to = receiver.address;
            balance = (await sender.GetXandBalance()).balanceInMinorUnit;

            try {
                await sender.transactionsApi.sendClaimsTransaction({
                    toAddress: to,
                    amountInMinorUnit: balance + 1,
                });
            } catch (err) {
                error = err as AxiosError;
            }
        });

        it("Then my request is rejected by the network", () => {
            // tslint:disable-next-line:max-line-length
            const errorMsg = error?.response?.data?.MemberService?.source?.XandClientError?.source?.BadRequest?.message;
            expect(errorMsg).to.equal("\"InsufficientClaims\"");
        });

        it("Then I can continue making valid transactions", async function() {
            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

            const receipt = await sender.SubmitSend(to, Math.floor(balance / 2));
            send = await sender.GetTransactionById(receipt.transactionId);

            expect(send).to.exist;

            expect(send.status.state).to.equal(TransactionState.Confirmed);

        });
    });

    describe("When I send claims to an invalid address", () => {

        let to: string;
        let balance: number;
        let error: AxiosError;

        before(async function() {
            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

            // this is a legit base-58 encoded address that does NOT
            // exist in our system.
            //
            to = "5EU2BwdEH8rojoKuJ16p3t1Gs8uUUp6SoaomKud2FmXBvdbp";
            balance = (await sender.GetXandBalance()).balanceInMinorUnit;

            try {
                await sender.transactionsApi.sendClaimsTransaction({
                    toAddress: to,
                    amountInMinorUnit: Math.floor(balance / 2),
                });
            } catch (err) {
                error = err as AxiosError;
            }
        });

        it("Then my request is rejected", () => {
            // tslint:disable-next-line:max-line-length
            const errorMsg = error?.response?.data?.MemberService?.source?.XandClientError?.source?.NotFound?.message;
            expect(errorMsg).to.equal("\"InvalidMember\"");
        });
    });
});
