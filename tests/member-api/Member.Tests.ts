import { expect } from "chai";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

describe("Given a Member named Awesomeco is interested in XAND balance", () => {
  const MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED = 120000;

  let context: TestContext;
  let awesomeCo: Member;
  let coolInc: Member;

  before(async () => {
    context = new TestContext();
    awesomeCo = await context.awesomeCo.EnsureBanksAreConfigured();
    coolInc = await context.coolInc.EnsureBanksAreConfigured();
  });

  describe("When I initialize the wallets with at least some claims", () => {
    // Passing arrow functions (“lambdas”) to Mocha is discouraged.
    // Due to the lexical binding of this, such functions are unable
    // to access the Mocha context.
    //
    // Translation: In order to call `this.timeout()` we need to use the
    // `async function` syntax instead of `async () =>` syntax.
    //
    // tslint:disable-next-line: only-arrow-functions
    before(async function() {
      this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

      await awesomeCo.SetWalletBalanceToAtLeast(20000);
      await coolInc.SetWalletBalanceToAtLeast(20000);
    });

    describe("And I check the balances", () => {
      it("Then awesomeco has money", async () => {
        const balance = await awesomeCo.GetXandBalance();
        expect(balance.balanceInMinorUnit).to.be.greaterThan(1000);
      });

      it("Then cool-inc has money", async () => {
        const balance = await coolInc.GetXandBalance();
        expect(balance.balanceInMinorUnit).to.be.greaterThan(1000);
      });
    });
  });
});
