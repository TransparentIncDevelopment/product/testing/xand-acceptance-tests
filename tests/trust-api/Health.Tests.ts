import { expect } from "chai";
import { HealthCheckStatus } from "xand-trust-api-client";
import TestContext from "../../lib/TestContext";

describe("Given a Trust API in a running XAND network", () => {
    let context: TestContext;

    before(() => {
        context = new TestContext();
    });

    describe("When I probe the health endpoint", () => {
        let healthResponse;
        before(async () => {
            healthResponse = await context.trust.GetHealth();
        });

        it("Then the health endpoint reports healthy", () => {
            expect(healthResponse.down).to.be.empty;
        });
        it("Then the health endpoint reports all the services are up", () => {
            const up = HealthCheckStatus.Up.toLowerCase();
            const isUp = (serviceHealth) => serviceHealth.status === up;
            expect(healthResponse.up.every(isUp)).to.be.true;
        });

    });
});
