import { expect } from "chai";
import some from "expect";

import Configuration from "../../lib/config/Configuration";

describe("Given a config file", () => {
  describe("When I load the default configuration", () => {
    let configuration: Configuration;

    before(() => {
      configuration = Configuration.default();
    });

    it("Then logging should be turned off.", () => {
      expect(configuration.logging).to.exist;
      expect(configuration.logging.url).to.equal(
        false,
        "Please turn off console logging before submitting an MR.",
      );
      expect(configuration.logging.request).to.equal(
        false,
        "Please turn off console logging before submitting an MR.",
      );
      expect(configuration.logging.response).to.equal(
        false,
        "Please turn off console logging before submitting an MR.",
      );
    });

    it("Then the members are populated.", () => {
      expect(configuration.members).to.exist;
    });

    it("Then the awesome member can be found.", () => {
      const awesomeCo = configuration.getAwesomeCo();
      some(awesomeCo).toMatchObject({
        id: "awesomeco",
        xngIndex: 0,
        banks: [
          {
            name: "MCB",
            adapter: {
              mcb: {
                url: "http://localhost:8888/metropolitan",
              },
            },
          },
          {
            name: "Provident",
            adapter: {
              treasuryPrime: {
                url: "http://localhost:8888/provident",
              },
            },
          },
        ],
      });
    });

    it("Then polling settings are specified.", () => {
      expect(configuration.polling.retries).to.equal(120);
      expect(configuration.polling.delay).to.equal(1000);
    });
  });

  describe("When using the local test environment", () => {
    it("Then the default configuration and loaded configuration should be the same", function() {
      if (Configuration.environment() === "local") {
        // This will not be the case when running against the docker network so ignore.
        const actual = Configuration.loadTestEnvironment();
        const expected = Configuration.default();
        expect(expected).to.deep.eq(actual);
      } else {
        // don't run this test in the docker environment.
        this.skip();
      }
    });
  });

  describe("When the default config file is found at /config/confg.yaml", () => {
    it("Then the acceptance tests config file can be loaded.", () => {
      const configuration = Configuration.default();
      expect(configuration).to.exist;
    });
  });

  describe("When I load the docker configuration", () => {
    let configuration: Configuration;

    before(() => {
      configuration = Configuration.override("docker");
    });

    it("Then logging should be turned off.", () => {
      expect(configuration.logging).to.exist;
      expect(configuration.logging.url).to.equal(
        false,
        "Please turn off console logging before submitting an MR.",
      );
      expect(configuration.logging.request).to.equal(
        false,
        "Please turn off console logging before submitting an MR.",
      );
      expect(configuration.logging.response).to.equal(
        false,
        "Please turn off console logging before submitting an MR.",
      );
    });

    it("Then the members are populated.", () => {
      expect(configuration.members).to.exist;
    });

    it("Then the awesome member can be found.", () => {
      const awesomeCo = configuration.getAwesomeCo();
      some(awesomeCo).toMatchObject({
        id: "awesomeco",
        banks: [
          {
            name: "MCB",
            adapter: {
              mcb: {
                url: "http://bank-mocks:8888/metropolitan",
              },
            },
          },
          {
            name: "Provident",
            adapter: {
              treasuryPrime: {
                url: "http://bank-mocks:8888/provident",
              },
            },
          },
        ],
      });
    });

    it("Then polling settings are specified.", () => {
      expect(configuration.polling.retries).to.equal(120);
      expect(configuration.polling.delay).to.equal(1000);
    });
  });
});
