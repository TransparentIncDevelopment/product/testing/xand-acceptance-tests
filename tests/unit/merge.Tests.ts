import { expect } from "chai";
import some from "expect";
import deepMerge from "../../lib/config/extend";

describe("Given a merge function", () => {

    it("can merge two simple objects", () => {
        const obj1 = {
          name: "fredbob",
          age: 42,
        };

        const obj2 = {
          name: "foobar",
          delta: "redbo",
        };

        const result = deepMerge(obj1, obj2);

        some(result).toMatchObject({
          name: "foobar",
          age: 42,
          delta: "redbo",
        });
      });

    it("can merge hierarchical object", () => {
        const obj1 = {
          name: "fredbob",
          age: 42,
          metadata: {
            day: 12,
            month: 12,
          },
        };

        const obj2 = {
          name: "foobar",
          delta: "redbo",
          metadata: {
            day: 15,
            year: 2020,
          },
        };

        const result = deepMerge(obj1, obj2);

        some(result).toMatchObject({
          name: "foobar",
          age: 42,
          delta: "redbo",
          metadata: {
            day: 15,
            month: 12,
            year: 2020,
          },
        });
      });

    it("can merge array property", () => {
        const obj1 = {
          name: "fredbob",
          age: 42,
          arr: ["super", "cali"],
        };

        const obj2 = {
          name: "foobar",
          delta: "redbo",
          arr: ["super", "awesome", "profile"],
        };

        const result = deepMerge(obj1, obj2);

        some(result).toMatchObject({
          name: "foobar",
          age: 42,
          delta: "redbo",
          arr: ["super", "awesome", "profile"],
        });
      });

    it("can merge array property of objects", () => {
        const obj1 = {
          name: "fredbob",
          arr: [
            { id: 1, name: "fred", rgb: "00000" },
            { id: 2, name: "bob", rgb: "11111" },
          ],
        };

        const obj2 = {
          name: "foobar",
          arr: [
            { id: 1, name: "foo", xray: 47 },
            { id: 19, name: "bob", xray: 52 },
            { test: true },
          ],
        };

        const result = deepMerge(obj1, obj2);

        some(result).toMatchObject({
          name: "foobar",
          arr: [
            {
              id: 1,
              name: "foo",
              rgb: "00000",
              xray: 47,
            },
            {
              id: 19,
              name: "bob",
              xray: 52,
              rgb: "11111",
            },
            {
              test: true,
            },
          ],
        });
      });

});
