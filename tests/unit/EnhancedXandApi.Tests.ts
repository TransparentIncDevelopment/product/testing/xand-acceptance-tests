import { expect } from "chai";
import { parseURlForGrpcClient } from "../../lib/EnhancedXandApi";

// nodejs grpc library does not support http(s) prefix: https://stackoverflow.com/a/62136381
// however, our network voting CLI requires the https prefix
// Reference https://transparentincdevelopment.gitlab.io/product/testing/xand-qa/members.html#setup
describe("Given a url for use with grpc", () => {
    it("Then the protocol is stripped", () => {
        // Given
        const url = "http://localhost:10044";

        // When
        const result = parseURlForGrpcClient(url);

        // Then
        expect(result).to.equal("localhost:10044");
    });

    it("Then any trailing slash is stripped", () => {
        // Given
        const url = "http://localhost:10044/";

        // When
        const result = parseURlForGrpcClient(url);

        // Then
        expect(result).to.equal("localhost:10044");
    });

    it("Then the port is maintained", () => {
        // Given
        const url = "http://localhost:443";

        // When
        const result = parseURlForGrpcClient(url);

        // Then
        expect(result).to.equal("localhost:443");
    });
});
