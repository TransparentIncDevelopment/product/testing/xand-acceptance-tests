import { expect } from "chai";

import XngMetadata from "../../lib/config/xng/XngMetadata";

describe("Given an entities-metadata file", () => {
    describe("When the default metadata file is found at /generated/entities-metadata.yaml", () => {
        it("Then the metadata file can be loaded.", () => {
            const metadata = XngMetadata.load_default();
            expect(metadata).to.exist;
        });
    });

    describe("When I load the default metadata file", () => {
        let metadata: XngMetadata;

        before(() => {
            metadata = XngMetadata.load_default();
        });

        it("Then the members are populated.", () => {
            expect(metadata.members).to.exist;
            expect(metadata.members).to.not.be.empty;
        });

        it("Then the 0th member's metadata is not null", () => {
            const zerothMember = metadata.members[0];
            expect(zerothMember).to.exist;
            expect(zerothMember.address).to.not.be.empty;
            expect(zerothMember.xandApiDetails.xandApiUrl).to.not.be.empty;
            expect(zerothMember.memberApiDetails.memberApiUrl).to.not.be.empty;
            expect(zerothMember.banks).to.not.be.empty;
        });

        it("Then the trust's metadata is not null", () => {
            const trust = metadata.trust;
            expect(trust).to.exist;
            expect(trust.address).to.not.be.empty;
            expect(trust.xandApiDetails.xandApiUrl).to.not.be.empty;
            expect(trust.trustApiUrl).to.not.be.empty;
        });

        it("Then the bank urls can be found", () => {
            const banks = metadata.banks;
            expect(banks.urls.mcb).to.equal("http://localhost:8888/metropolitan/");
            expect(banks.urls.provident).to.equal("http://localhost:8888/provident/");
        });

    });
});
