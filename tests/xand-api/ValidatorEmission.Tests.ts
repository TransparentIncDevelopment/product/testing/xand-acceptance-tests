import { expect } from "chai";
import TestContext from "../../lib/TestContext";
import Validator from "../../lib/Validator";

const REDEEM_AMOUNT = 100;

// tslint:disable-next-line:max-line-length
describe(`Given a validator with at least \$${REDEEM_AMOUNT / 100} of emission rewards`, () => {
    let context: TestContext;
    let validator: Validator;

    before(async function() {
        // One round takes 18 seconds to complete, and validators are rewarded every
        // 2 rounds (block quota = 2), but wait 3 rounds because of block jitter...
        const TIME_FOR_AT_LEAST_ONE_ROUND_OF_REWARDS = 54000;
        const MAX_TIME_TO_WAIT_FOR_REWARDS_BALANCE = TIME_FOR_AT_LEAST_ONE_ROUND_OF_REWARDS + 5000;
        this.timeout(MAX_TIME_TO_WAIT_FOR_REWARDS_BALANCE);

        context = new TestContext();
        validator = context.getValidator();

        if (await validator.getBalance() < REDEEM_AMOUNT) {
            await new Promise((res) => setTimeout(res, TIME_FOR_AT_LEAST_ONE_ROUND_OF_REWARDS));
            if (await validator.getBalance() < REDEEM_AMOUNT) {
                throw new Error(`Waited for ~one round of emissions but validator still has < ${REDEEM_AMOUNT} balance. Emissions may not be working`);
            }
        }
    });

    describe(`When a validator redeems \$${REDEEM_AMOUNT / 100}`, () => {
        let receipt;
        before(async function() {
            const MAX_TIME_TO_WAIT_ON_FULFILLMENT = 120000;
            this.timeout(MAX_TIME_TO_WAIT_ON_FULFILLMENT);

            receipt = await validator.redeem(REDEEM_AMOUNT);
            await validator.awaitRedeemCompletion(receipt.redeemRequest);
        });

        it("Then the redemption is confirmed", async () => {
            const redeemRequest = await validator.getRedeemRequest(receipt.redeemRequest);
            expect(redeemRequest.getCompletingTransaction().hasConfirmation()).to.be.true;
        });
    });

    describe(`When a validator redeems with incorrect bank account specified`, () => {
        let receipt;
        before(async function() {
            const MAX_TIME_TO_WAIT_ON_FULFILLMENT = 120000;
            this.timeout(MAX_TIME_TO_WAIT_ON_FULFILLMENT);

            receipt = await validator.redeem(REDEEM_AMOUNT, "badAccount");
            await validator.awaitRedeemCompletion(receipt.redeemRequest);
        });

        it("Then the redemption is cancelled", async () => {
            const redeemRequest = await validator.getRedeemRequest(receipt.redeemRequest);
            expect(redeemRequest.getCompletingTransaction().hasCancellation()).to.be.true;
        });
    });
});
